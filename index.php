<?php

require_once('animal.php');
require_once('Frog.php');
require_once('ape.php');

$object = new animal("shaun");

echo "Name : " . $object -> name . "<br>";
echo "Legs : " .$object -> legs . "<br>";
echo "Cold Blooded : " . $object -> cold_blooded . "<br>";

$object2 = new frog("buduk");
echo " <br>Name : " . $object2 -> name . "<br>";
echo "Legs : " .$object2 -> legs . "<br>";
echo "Cold Blooded : " . $object2 -> cold_blooded . "<br>";
$object2->jump();

$object3 = new ape("Kera Sakti");
echo " <br><br>Name : " . $object3 -> name . "<br>";
echo "Legs : " .$object3 -> legs . "<br>";
echo "Cold Blooded : " . $object3 -> cold_blooded . "<br>";
$object3->yell();

?>
